raizes(X,Y,A,B,C) :-
sqrt(B*B-4*A*C) =:= 0 -> X is -B/2*A;
sqrt(B*B-4*A*C) > 0 -> X is (-B+sqrt(B*B-4*A*C))/2*A,Y is (-B-sqrt(B*B-4*A*C))/2*A;
sqrt(B*B-4*A*C) < 0 -> writeln('não há raízes').